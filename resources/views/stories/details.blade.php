@extends('layouts.main')

@section('content')

<div class="an-inner-banner">

        <div class="container">
          <div class="an-post-title">
            <span class="an-badge default">ABOUT</span>
            <div class="an-title-container left">
              <h1 class="an-title">{{$story->title}}</h1>
            </div> <!-- end title container -->
            <div class="listing-meta">
              <span class="user"><span class="meta-italic">By:</span><a href="#">{{$story->name}}</a></span>
              <span><i class="typcn typcn-eye-outline color-job"></i> 10 234</span>
              <span><i class="typcn typcn-heart-outline color-accents"></i> 234</span>
            </div>
          </div>

        </div> <!-- end cotnainer -->
      </div> <!-- an-header-banner -->

      <div class="an-page-content">
        <div class="container">
          <div class="row">
            <div class="col-md-9">
              <div class="content-body">
                <div class="an-section-container pb15">
                  <div class="an-blog-post">
                    <div class="post-img">
                      <img src="{{asset('img/blog/p2.jpg')}}" alt="">
                    </div>
                    <div class="content">
                      {!!$story->story!!}

                    </div> <!-- end content -->

                  </div>
                </div> <!-- end an-section-container -->
              </div> <!-- end content-body -->
            </div>
            <div class="col-md-3">
              <div class="an-sidebar mt60">
                <div class="widget-author">
                  <div class="an-testimonial">
                    <div class="testimonial-single">
                      <div class="image" style="background: url('{{asset('img/users/user1.jpg')}}') center center no-repeat; background-size: cover;"></div>
                      <p class="accents">Author</p>
                      <a href="#">{{$story->name}}</a>

                      <div class="listing-meta">
                        <span>USA, knoxville</span>
                        <span>123 400 500 600</span>
                        <span>example@example.com</span>
                      </div>

                      <a class="an-link-icon-btn" href="#"><i class="ion-ios-paperplane"></i></a>
                    </div> <!-- end testimonial-single -->
                  </div>

                </div> <!-- end and-widget-author -->


              </div> <!-- end an-sidebar -->
            </div> <!-- end col-md-3 -->
          </div>
        </div>
      </div> <!-- end an-page-content -->

@endsection
