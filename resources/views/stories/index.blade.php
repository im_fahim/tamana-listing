@extends('layouts.main')

@section('content')
<div class="an-inner-banner has-bg" style="background: url('{{asset('img/slider3.jpg')}}') center center no-repeat;
       background-size: cover;">
       <div class="overlay"></div>

       <div class="container">
         <div class="an-title-container">
           <h1 class="an-title">Stories</h1>
           <ol class="breadcrumb">
             <li><a href="#">Home</a></li>
             <li class="active">Stories</li>
           </ol>

         </div> <!-- end title container -->

       </div> <!-- end cotnainer -->
     </div> <!-- an-header-banner -->

     <div class="an-page-content">
       <div class="container">
         <div class="row">
           <div class="col-md-12">
             <div class="content-body">
               <div class="an-section-container pb15">
                 <div class="an-blog-container">
                   <div class="row">

                     @foreach($stories as $story)
                      <div class="col-md-6">
                        <div class="an-blog-single"
                          style="background: url('{{asset('img/blog/p9.jpg')}}') center center no-repeat;
                          background-size: cover;">
                          <div class="overlay"></div>
                          <div class="content">
                            <h3><a href="{{route('stories.details',[$story->id])}}">{{$story->title}}</a></h3>
                            <div class="listing-meta">
                              <span class="user"><span class="meta-italic">By:</span><a href="#">{{$story->name}}</a></span>
                              <span><i class="ion-android-time"></i>16.1.17</span>
                            </div>
                          </div>
                        </div>
                      </div>
                        @endforeach

                   </div> <!-- end row -->
                 </div> <!-- / blog-container -->
               </div> <!-- end an-section-container -->
             </div> <!-- end content-body -->
           </div>

         </div>
       </div>
     </div> <!-- end an-page-content -->

@endsection
