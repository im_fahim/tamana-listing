<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from analoglyf.com/estate/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 01 Feb 2018 09:58:43 GMT -->
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>login</title>
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('css/vendor-styles.css')}}">
    <link rel="stylesheet" href="{{asset('css/styles.css')}}">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div id="an-main-wrapper">
      <div class="an-login-register-content">
        <div class="content">
          <a class="logo" href="index.html"><img src="{{asset('img/logo-primary.png')}}" alt=""></a>
          <h1>Welcome to Estate</h1>

          <form class="an-form" method="POST" action="{{ route('login') }}">
            {{csrf_field()}}
            <div class="row">
              <div class="col-sm-12">
                <input type="email" class="an-form-control" placeholder="E-mail" name="email" value="{{ old('email') }}" required autofocus>
              </div>
              <div class="col-sm-12">
                <input type="password" class="an-form-control" placeholder="Password" name="password" required>
              </div>
            </div>
            <button type="submit" class="an-btn an-btn-default large-padding">Login</button>
          </form>
        </div>
      </div>

      <div class="an-loader">
        <div class="fl spinner2">
          <div class="spinner-container container1">
            <div class="circle1"></div>
            <div class="circle2"></div>
            <div class="circle3"></div>
            <div class="circle4"></div>
          </div>
          <div class="spinner-container container2">
            <div class="circle1"></div>
            <div class="circle2"></div>
            <div class="circle3"></div>
            <div class="circle4"></div>
          </div>
          <div class="spinner-container container3">
            <div class="circle1"></div>
            <div class="circle2"></div>
            <div class="circle3"></div>
            <div class="circle4"></div>
          </div>
        </div>
      </div>
    </div> <!-- end main wrapper -->
    <script src="{{asset('js-plugins/jquery-3.1.1.min.js')}}" type="text/javascript"></script>

    <script src="{{asset('js-plugins/bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js-plugins/selectize.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js-plugins/jquery-ui.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js-plugins/owl.carousel.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/scripts.js')}}" type="text/javascript"></script>
  </body>

<!-- Mirrored from analoglyf.com/estate/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 01 Feb 2018 09:58:43 GMT -->
</html>
