@extends('layouts.main')

@section('content')


<div class="an-inner-banner">
        <div class="container">
          <div class="an-agency-single an-agent-single">
            <div class="left">
              <div class="image" style="background: url('assets/img/users/user1.jpg') center center no-repeat; background-size: cover;">
              </div>

              <div class="name">
                <div class="an-title-container center">
                  <h1 class="an-title"><a href="#">{{$data->name}}</a></h1>
                  <ul class="list-inline category-list-alter">
                      @foreach(json_decode($data->specialities , true) as $value)
                      <li><a href="#"><i class="ion-record color-primary"></i>{{ $value }}</a></li>
                      @endforeach
                  </ul>
                </div> <!-- end title container -->
              </div>

              <div class="listing-meta">
                <span><i class="ion-android-call"></i>{{$data->contact_no}}</span>
                <span><i class="typcn typcn-location"></i>{!!$data->location!!}</span>
                <span><i class="ion-android-mail"></i>{{$data->email}}</span>
              </div>
              <div class="listing-count">
                <a class="an-btn an-btn-default icon-right" href="#">Follow <i class="ion-person-add"></i></a>
                <a class="an-btn an-btn-default icon-right" href="#">Send message <i class="ion-paper-airplane"></i></a>
              </div>
            </div>
          </div> <!-- end agency-single -->

        </div> <!-- end cotnainer -->
      </div> <!-- an-header-banner -->

      <div class="an-page-content">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="content-body">
                <div class="an-section-container no-bottom-padding">
                  <div class="an-agent-about">
                    <h2 class=page-title>Working With</h2>
                    <p class="an-quote">
                      @foreach(json_decode($data->challanges , true) as $value)
                      {{$value }}&nbsp;|
                      @endforeach
                    </p>
                  </div> <!-- end an-agent-about -->
                            </div>
                          </div>

                        </div> <!-- end tab pane -->

          </div>
                </div>

                </div>
          </div>
        </div>
      </div> <!-- end an-page-content -->


@endsection
