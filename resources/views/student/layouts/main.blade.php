<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    @include('student.layouts.head')

    @include('student.layouts.styles')

</head>

<body>
    <div class="wrapper">
        <div class="sidebar" data-color="orange" data-image="{{asset('student/assets/img/sidebar-1.jpg')}}">
            <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

        Tip 2: you can also add an image using data-image tag
    -->
          @include('student.layouts.sidebar')

        </div>
        <div class="main-panel">
            @include('student.layouts.nav')

            <div class="content">
              @include('student.layouts.messages')

        			@yield('content')
      			</div>

            @include('student.layouts.footer')
        </div>
    </div>
</body>

@include('student.layouts.scripts')

@yield('page-scripts')

</html>
