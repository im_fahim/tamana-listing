@extends('layouts.main')

@section('content')
      <div class="an-inner-banner has-bg" style="background: url({{asset('img/slider2.jpg')}}) center center no-repeat;
        background-size: cover;">
        <div class="overlay"></div>

        <div class="container">
          <div class="an-title-container">
            <h1 class="an-title">Contact</h1>
            <ol class="breadcrumb">
              <li><a href="/">Home</a></li>
              <li class="active">Contact</li>
            </ol>

          </div> <!-- end title container -->

        </div> <!-- end cotnainer -->
      </div> <!-- an-header-banner -->


      <div class="an-page-content">
        <div class="an-contact-us-content">
          <div class="container">
            <div class="row">
              <div class="col-md-6">
                <div class="contact-details">
                  <div class="address-single">
                    <i class="ion-android-pin"></i>
                    <h4 class="an-small-title">Address</h4>
                    <p>115 The Vale, London, W3 7QR</p>
                  </div>

                  <div class="address-single">
                    <i class="ion-android-call"></i>
                    <h4 class="an-small-title">Telephone</h4>
                    <p>+971 1234 3456</p>
                  </div>

                  <div class="address-single">
                    <i class="ion-android-mail"></i>
                    <h4 class="an-small-title">Email</h4>
                    <p>example@example.com</p>
                  </div>

                  <div class="address-single">
                    <i class="ion-android-time"></i>
                    <h4 class="an-small-title">Opening hours</h4>
                    <p>Snd - Trd <span>08.00 - 20.00</span></p>
                    <p>Frd - Strd <span>12.00 - 20.00</span></p>
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="contact-details">
                  <h4 class="an-small-title">Send Us Message</h4>
                  <form action="#" class="an-form">
                    <div class="row">
                      <div class="col-sm-6">
                        <input class="an-form-control" type="text" placeholder="Your name">
                      </div>
                      <div class="col-sm-6">
                        <input class="an-form-control" type="text" placeholder="Your email">
                      </div>
                      <div class="col-sm-12">
                        <textarea class="an-form-control" placeholder="Type your message"></textarea>
                      </div>
                      <div class="col-sm-12">
                        <button class="an-btn an-btn-default">Send Message</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <div class="contact-map-content">
            <div id="contact-map"></div>
          </div>
        </div> <!-- end an-contact-us-content -->
      </div> <!-- end an-page-content -->

@endsection
