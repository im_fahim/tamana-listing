@extends('layouts.main')

@section('content')
      <div class="an-inner-banner has-bg"
        style="background: url({{asset('img/slider3.jpg')}}) center center no-repeat;background-size: cover;">
        <div class="overlay"></div>
        <div class="container">
          <div class="an-agency-single an-agent-single">
            <div class="left">

              <div class="name">
                <div class="an-title-container center">
                  <h1 class="an-title"><a href="#">TAMANA</a></h1>
                </div> <!-- end title container -->
              </div>

              
              <div class="listing-count">
                <a class="an-btn an-btn-default icon-right" href="#" style="background-color: #4267B2; border: none;">Facebook</a>
                <a class="an-btn an-btn-default icon-right" href="#" style="background-color: #1DA1F2; border: none;">Twitter</i></a>
                <a class="an-btn an-btn-default icon-right" href="#" style="background-color: #FF0000; border: none;">Youtube</i></a>
              </div>
            </div>
          </div> <!-- end agency-single -->

        </div> <!-- end cotnainer -->
      </div> <!-- an-header-banner -->

      <div class="an-page-content">
        <div class="container">
          <div class="row">
            <div class="col-md-9">
              <div class="content-body">
                <div class="an-section-container no-bottom-padding">
                  <div class="an-agent-about">
                    <h2 class=page-title>About</h2>
                    <p class="an-quote">
                      It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here.
                    </p>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here.It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here.</p>
                  </div> <!-- end an-agent-about -->

                </div>
              </div>
            </div>
          </div>
        </div>
      </div> <!-- end an-page-content -->
@endsection
