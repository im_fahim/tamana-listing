<footer class="an-footer">
  <div class="container">
    <div class="an-footer-bottom">
      <div class="row">
        <div class="col-md-2 col-sm-6">
          <div class="an-widget">
            <h4 class="an-small-title light-color"></h4>
            <ul>
              <li><a href="/">Home</a></li>
              <li><a href="{{route('about')}}">About</a></li>
              <li><a href="{{route('contact')}}">Contact</a></li>
            </ul>
          </div>
        </div>
        <div class="col-md-2 col-sm-6">
          <div class="an-widget">
            <h4 class="an-small-title light-color"></h4>
            <ul>
              <li><a href="{{route('narratives')}}">Narratives</a></li>
              <li><a href="#">Stories</a></li>

            </ul>
          </div>
        </div>

        <div class="col-md-2 col-sm-6">
          <div class="an-widget">
            <h4 class="an-small-title light-color"></h4>
            <ul>
              <li><a href="#">Services</a></li>
              <li><a href="#">Events</a></li>
            </ul>
          </div>
        </div>
        <div class="col-md-2 col-sm-6">
          <div class="an-widget">
            <h4 class="an-small-title light-color">Quick Links</h4>
            <ul>
              <li><a href="#">Dashboard</a></li>
              <li><a href="#">Profile</a></li>
              <li><a href="#">Agency Profile</a></li>
              <li><a href="#">Activity</a></li>
            </ul>
          </div>
        </div>
        <div class="col-md-3 col-md-push-1 col-sm-6">
          <div class="an-widget newsletter">
            <h4 class="an-small-title light-color">Subscribe to our news</h4>
            <p>Contrary to popular belief, Lorem Ipsum is not simply random text.
              It has roots in a piece of classical Latin.</p>
            <form action="#" class="an-form">
              <input type="email" class="an-form-control dark" placeholder="E-mail">
              <button type="submit" class="an-btn an-btn-default btn-submit-full"><i class="ion-ios-paperplane"></i></button>
            </form>

          </div>
        </div>
      </div>
    </div>
  </div> <!-- end container -->
  <div class="an-footer-copyright">
    <div class="container">
      <div class="content">
        <p class="copyrights">© 2017 Estate Property Listing by analoglife. All rights reserved</p>
        <ul class="an-social-icons">
          <li><a href="#"><i class="fa fa-twitter"></i></a></li>
          <li><a href="#"><i class="fa fa-facebook"></i></a></li>
          <li><a href="#"><i class="fa fa-youtube"></i></a></li>
        </ul>

      </div>
    </div>
  </div>
</footer> <!-- end an-footer -->
<div class="an-loader">
  <div class="fl spinner2">
    <div class="spinner-container container1">
      <div class="circle1"></div>
      <div class="circle2"></div>
      <div class="circle3"></div>
      <div class="circle4"></div>
    </div>
    <div class="spinner-container container2">
      <div class="circle1"></div>
      <div class="circle2"></div>
      <div class="circle3"></div>
      <div class="circle4"></div>
    </div>
    <div class="spinner-container container3">
      <div class="circle1"></div>
      <div class="circle2"></div>
      <div class="circle3"></div>
      <div class="circle4"></div>
    </div>
  </div>
</div>
</div> <!-- end main wrapper -->
<script src="{{asset('js-plugins/jquery-3.1.1.min.js')}}" type="text/javascript"></script>

<script src="{{asset('js-plugins/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js-plugins/selectize.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js-plugins/jquery-ui.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js-plugins/owl.carousel.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js-plugins/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js-plugins/daterangepicker.js')}}" type="text/javascript"></script>

<script src="{{asset('js/scripts.js')}}" type="text/javascript"></script>

</body>

<!-- Mirrored from analoglyf.com/estate/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 01 Feb 2018 09:58:42 GMT -->
</html>
