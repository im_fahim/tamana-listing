
  <body>
    <div id="an-main-wrapper">
      <header class="an-header">
        <nav class="navbar navbar-default home-nav">
          <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand default" href="/"><img src="{{asset('img/logo-primary.png')}}" alt=""></a>
              <a class="navbar-brand sticky" href="index.html"><img src="{{asset('img/logo-new.png')}}" alt=""></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="/">Home</a></li>
                <li><a href="{{route('about')}}">About</a></li>
                <li><a href="{{route('narratives')}}">Narratives</a></li>
                <li><a href="{{route('stories.index')}}">Stories</a></li>
                <li><a href="contact.html">Services</a></li>
                <li><a href="{{route('events.details')}}">Events</a></li>
                <li><a href="{{route('contact')}}">Contact</a></li>
                <!--<li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Listing<span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="listing.html">Listing</a></li>
                    <li><a href="listing-right-map.html">Listing right map</a></li>
                    <li><a href="listing-single.html">Listing single</a></li>
                  </ul>
                </li>-->

                <!--<li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Agents<span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="agents.html">Agent listing</a></li>
                    <li><a href="agent-profile.html">Agent profile</a></li>
                    <li><a href="agent-profile-2.html">Agent profile banner</a></li>
                  </ul>
                </li>-->

                <!--<li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Pages<span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="blog.html">Blog</a></li>
                    <li><a href="blog-post.html">Blog post</a></li>
                    <li><a href="pricing.html">Pricing table</a></li>
                    <li><a href="login.html">Login</a></li>
                    <li><a href="register.html">Register</a></li>
                    <li><a href="elements.html">Elements</a></li>
                    <li><a href="404.html">404 page</a></li>
                    <li><a href="agent-edit-profile.html">Edit profile</a></li>
                  </ul>
                </li>-->

              </ul>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>
      </header> <!-- end an-header -->
