@extends('layouts.main')

@section('content')
<div class="an-inner-banner has-bg" style="background: url('{{asset('img/slider3.jpg')}}') center center no-repeat;
        background-size: cover;">
        <div class="overlay"></div>

        <div class="container">
          <div class="an-title-container">
            <h1 class="an-title">All Agents</h1>
            <ol class="breadcrumb">
              <li><a href="#">Home</a></li>
              <li class="active">Agents</li>
            </ol>

          </div> <!-- end title container -->

        </div> <!-- end cotnainer -->
      </div> <!-- an-header-banner -->
<pre>
</pre>
      <div class="an-page-content">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="content-body pb0">
                <div class="an-section-container pb0">
                  <!--<div class="an-search-container">-->
                    <div class="an-tab-container">
                      <div class="tab-nav">
                        <ul class="nav nav-tabs" role="tablist">
                          <li role="presentation" class="active"><a href="#professional" aria-controls="professional" role="tab" data-toggle="tab">Professionals</a></li>
                          <li role="presentation"><a href="#institution" aria-controls="institution" role="tab" data-toggle="tab">Institutions</a></li>
                          <li role="presentation"><a href="#support" aria-controls="support" role="tab" data-toggle="tab">Support</a></li>
                        </ul>
                      </div>

                      <!-- Tab panes -->
                      <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="professional">
                          <form action="/search" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="type" value="professional">
                            <div class="search-fields">
                              <div class="row">
                                <div class="col-md-4">
                                  <input class='an-form-control' type="text" name="location" placeholder="Enter location">
                                </div>
                                <div class="col-md-4">
                                  <div class="an-default-select-wrapper">
                                    <select name="challange">
                                      <option value="">--Select (Optional)--</option>
                                      <option value="Intellectual Disability">Intellectual Disability</option>
                                      <option value="Autism">Autism</option>
                                      <option value="Down Syndrome">Down Syndrome</option>
                                      <option value="Cerebral Palsy">Cerebral Palsy</option>
                                    </select>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <input class='an-form-control' type="text" name="name" placeholder="Professional's Name">
                                </div>
                                <div class="col-md-12" style="margin-bottom: 10px;">
                                  <div class="element-single">
                                    <div class="col-md-2">
                                      <h4 style="font-weight:bold;text-align:left;">Services:</h4>
                                    </div>
                                    <div class="col-md-2">
                                    <span class="an-custom-checkbox radio dark-text">
                                      <input type="radio" name="service" id="check-111" value="" checked>
                                      <label for="check-111">All</label>
                                    </span>
                                  </div>
                                    <div class="col-md-2">
                                    <span class="an-custom-checkbox radio dark-text">
                                      <input type="radio" name="service" id="check-112" value="Occupational Therapy">
                                      <label for="check-112">Occupational Therapy</label>
                                    </span>
                                    </div>
                                    <div class="col-md-2">
                                    <span class="an-custom-checkbox radio dark-text">
                                      <input type="radio" name="service" id="check-113" value="Speech Therapy">
                                      <label for="check-113">Speech Therapy</label>
                                    </span>
                                    </div>
                                    <div class="col-md-2">
                                    <span class="an-custom-checkbox radio dark-text">
                                      <input type="radio" name="service" id="check-114" value="Psychiatry">
                                      <label for="check-114">Psychiatry</label>
                                    </span>
                                    </div>
                                    <div class="col-md-2">
                                    <span class="an-custom-checkbox radio dark-text">
                                      <input type="radio" name="service" id="check-115" value="Physiotherapy">
                                      <label for="check-115">Physiotherapy</label>
                                    </span>
                                    </div>
                                    </div>
                                  </div>
                                    <div class="col-md-2 col-md-offset-2">
                                    <span class="an-custom-checkbox radio dark-text">
                                      <input type="radio" name="service" id="check-116" value="Paediatricians">
                                      <label for="check-116">Paediatricians</label>
                                    </span>
                                    </div>
                                <div class="col-sm-12">
                                  <button type="submit" class="an-btn an-btn-default icon-left fluid"><i class="fa fa-search"></i>Search</button>
                                </div>
                                <!-- end nested row -->
                              </div>
                            </div>
                          </form>
                        </div> <!-- end tab pane -->
                        <div role="tabpanel" class="tab-pane fade rent-pane" id="institution">
                          <form action="/search" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="type" value="institution">
                            <div class="search-fields">
                              <div class="row">
                                <div class="col-md-4">
                                  <input class='an-form-control' type="text" name="location" placeholder="Enter location">
                                </div>
                                <div class="col-md-4">
                                  <div class="an-default-select-wrapper">
                                    <select name="challange">
                                      <option value="">--Select (Optional)--</option>
                                      <option value="Intellectual Disability">Intellectual Disability</option>
                                      <option value="Autism">Autism</option>
                                      <option value="Down Syndrome">Down Syndrome</option>
                                      <option value="Cerebral Palsy">Cerebral Palsy</option>
                                    </select>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <input class='an-form-control' type="text" name="name" placeholder="Institution's Name">
                                </div>
                                <div class="col-md-12" style="margin-bottom: 10px;">
                                  <div class="element-single">
                                    <div class="col-md-2">
                                      <h4 style="font-weight:bold;text-align:left;">Services:</h4>
                                    </div>
                                    <div class="col-md-2">
                                    <span class="an-custom-checkbox radio dark-text">
                                      <input type="radio" name="service" id="check-211" value="" checked>
                                      <label for="check-211">All</label>
                                    </span>
                                  </div>
                                    <div class="col-md-2">
                                    <span class="an-custom-checkbox radio dark-text">
                                      <input type="radio" name="service" value="School" id="check-212">
                                      <label for="check-212">School</label>
                                    </span>
                                    </div>
                                    <div class="col-md-2">
                                    <span class="an-custom-checkbox radio dark-text">
                                      <input type="radio" name="service" value="Residence" id="check-213">
                                      <label for="check-213">Residence</label>
                                    </span>
                                    </div>
                                    <div class="col-md-2">
                                    <span class="an-custom-checkbox radio dark-text">
                                      <input type="radio" name="service" value="Day Care" id="check-214">
                                      <label for="check-214">Day Care</label>
                                    </span>
                                    </div>
                                    <div class="col-md-2">
                                    <span class="an-custom-checkbox radio dark-text">
                                      <input type="radio" name="service" value="Skill Development" id="check-215">
                                      <label for="check-215">Skill Development</label>
                                    </span>
                                    </div>
                                  </div>
                                </div>
                                    <div class="col-md-2 col-md-offset-2">
                                    <span class="an-custom-checkbox radio dark-text">
                                      <input type="radio" name="service" value="Diagnostic Center" id="check-216">
                                      <label for="check-216">Diagnostic Center</label>
                                    </span>
                                    </div>
                                    <div class="col-md-2">
                                    <span class="an-custom-checkbox radio dark-text">
                                      <input type="radio" name="service" value="Early Intervention" id="check-217">
                                      <label for="check-217">Early Intervention</label>
                                    </span>
                                    </div>


                                <div class="col-sm-12">
                                  <button type="submit" class="an-btn an-btn-default icon-left fluid"><i class="fa fa-search"></i>Search</button>
                                </div>
                                <!-- end nested row -->
                              </div>
                            </div>
                          </form>
                        </div>
                      <div role="tabpanel" class="tab-pane fade rent-pane" id="support">
                        <form action="/search" method="POST">
                          {{ csrf_field() }}
                          <input type="hidden" name="type" value="support">
                          <div class="search-fields">
                            <div class="row">
                              <div class="col-md-4">
                                <input class='an-form-control' type="text" name="location" placeholder="Enter location">
                              </div>
                              <div class="col-md-4">
                                <div class="an-default-select-wrapper">
                                  <select name="challange">
                                    <option value="">--Select (Optional)--</option>
                                    <option value="Intellectual Disability">Intellectual Disability</option>
                                    <option value="Autism">Autism</option>
                                    <option value="Down Syndrome">Down Syndrome</option>
                                    <option value="Cerebral Palsy">Cerebral Palsy</option>
                                  </select>
                                </div>
                              </div>
                              <div class="col-md-4">
                                <input class='an-form-control' type="text" name="name" placeholder="Professional's Name">
                              </div>
                              <div class="col-md-12" style="margin-bottom: 10px;">
                                <div class="element-single">
                                  <div class="col-md-2">
                                    <h4 style="font-weight:bold;text-align:left;">Services:</h4>
                                  </div>
                                  <div class="col-md-2">
                                  <span class="an-custom-checkbox radio dark-text">
                                    <input type="radio" name="service" value="" id="check-311" checked>
                                    <label for="check-311">All</label>
                                  </span>
                                </div>
                                  <div class="col-md-2">
                                  <span class="an-custom-checkbox radio dark-text">
                                    <input type="radio" name="service" value="Counselling" id="check-312">
                                    <label for="check-312">Counselling</label>
                                  </span>
                                  </div>
                                  <div class="col-md-2">
                                  <span class="an-custom-checkbox radio dark-text">
                                    <input type="radio" name="service" value="Lawywers and Advocates" id="check-313">
                                    <label for="check-313">Lawywers and Advocates</label>
                                  </span>
                                  </div>
                                  <div class="col-md-2">
                                  <span class="an-custom-checkbox radio dark-text">
                                    <input type="radio" name="service" value="Daycare Centres" id="check-314">
                                    <label for="check-314">Daycare Centres</label>
                                  </span>
                                  </div>
                                  <div class="col-md-2">
                                  <span class="an-custom-checkbox radio dark-text">
                                    <input type="radio" name="service" value="Parents Support Group" id="check-315">
                                    <label for="check-315">Parents Support Group</label>
                                  </span>
                                  </div>
                                </div>
                                </div>
                                  <div class="col-md-2 col-md-offset-2">
                                  <span class="an-custom-checkbox radio dark-text">
                                    <input type="radio" name="service" value="Special Education" id="check-316">
                                    <label for="check-316">Special Education</label>
                                  </span>
                                  </div>

                              <div class="col-sm-12">
                                <button type="submit" class="an-btn an-btn-default icon-left fluid"><i class="fa fa-search"></i>Search</button>
                              </div>
                              <!-- end nested row -->

                          </div>
                        </form>
                      </div> <!-- end tab pane -->

                    </div> <!-- an-tab-container -->

                  </div> <!-- end an-search-container -->

               <!-- end an-header-banner -->

          <!--  <div class="an-page-content">
              <div class="an-section-container">
                <div class="container">
                  <div class="an-title-container">
                    <h1 class="an-title">Featured Locations</h1>
                    <p class="an-sub-title">Our top location list based on sale and rent property.</p>
                  </div> --><!-- end title container -->

                <!-- <div class="an-location-listing">
                    <div class="row">
                      <div class="an-location-single col-md-3 col-sm-4">
                        <a href="#">
                          <div class="content" style="background: url('{{asset("img/paris.jpg")}}') center center no-repeat;
                            background-size: cover;">
                            <h2>Paris</h2>
                          </div> --><!-- end content -->
                      <!--  </a>
                      </div>--> <!-- end an-location -single -->
                      <!--
                      <div class="an-location-single col-md-3 col-sm-4">
                        <a href="#">
                          <div class="content" style="background: url('{{asset("img/london.jpg")}}') center center no-repeat;
                            background-size: cover;">
                            <h2>London</h2>
                          </div>--> <!-- end content -->
                      <!--  </a>
                      </div> --><!-- end an-location -single -->

                    <!--  <div class="an-location-single col-md-3 col-sm-4">
                        <a href="#">
                          <div class="content" style="background: url('{{asset("img/spain.jpg")}}') center center no-repeat;
                            background-size: cover;">
                            <h2>Spain</h2>
                          </div>--> <!-- end content -->
                      <!--  </a>
                      </div>--> <!-- end an-location -single -->

                      <!--<div class="an-location-single col-md-3 col-sm-4">
                        <a href="#">
                          <div class="content" style="background: url('{{asset("img/new-york.jpg")}}') center center no-repeat;
                            background-size: cover;">
                            <h2>New York</h2>
                          </div>--> <!-- end content -->
                        </a>
                      </div> <!-- end an-location -single -->
                    <!--</div>-->

                    <br /><br />
                  <div class="an-tab-container">
                    <div class="tab-nav">
                      <h3 class="results">{{count($data)}} {{$type}}s found</h3>
                    </div>

                    <!-- Tab panes -->
                    <div class="tab-content">
                      <div role="tabpanel" class="tab-pane fade in active" id="all">
                        <div class="an-agencies an-agents">
                          <div class="row">




                            @foreach($data as $dt)
                            <div class="col-md-4 col-sm-4">
                              <div class="an-agency-single an-agent-single">
                                <div class="left">
                                  <div class="image" style="background: url('{{asset('img/users/user2.jpg')}}') center center no-repeat; background-size: cover;">
                                    <div class="overlay">
                                      <a class="message" href="#"><i class="ion-ios-paperplane"></i></a>
                                      <a class="link" href="agent-profile.html"><i class="ion-forward"></i></a>
                                    </div>
                                  </div>

                                  <div class="name">
                                    <h3><a href="agent-profile.html">{{ $dt->name }}</a><span>Pro</span></h3>
                                    <ul class="list-inline category-list-alter">

                                        @foreach(json_decode($dt->specialities , true) as $value)
                                        <li><a href="#"><i class="ion-record color-primary"></i>{{ $value }}</a></li>
                                        @endforeach
                                    </ul>
                                  </div>

                                  <div class="listing-meta">
                                    <span><i class="typcn typcn-location"></i>{{$dt->address}}</span>
                                  </div>
                                  <div class="listing-count">
                                    <a class="an-btn an-btn-default btn-sm rounded" href="{{route('details',[$dt->id,$type])}}">Details</a>
                                  </div>
                                </div>
                              </div> <!-- end agency-single -->
                            </div>
                            @endforeach


                            </div>
                          </div> <!-- end row -->
                        </div> <!-- end an-agencies -->
                      </div> <!-- end tab pane -->
                    </div> <!-- end tab-content -->
                  </div>
                </div> <!-- end an-section-container -->
              </div> <!-- end content-body -->
            </div>
          </div>
        </div>
      </div> <!-- end an-page-content -->

@endsection
