@extends('layouts.main')

@section('content')
      <div class="an-inner-banner has-bg" style="background: url({{asset('img/slider3.jpg')}}) center center no-repeat;
        background-size: cover;">
        <div class="overlay"></div>

        <div class="container">
          <div class="an-title-container">
            <h1 class="an-title">Stories</h1>
            <ol class="breadcrumb">
              <li><a href="#">Home</a></li>
              <li class="active">Stories</li>
            </ol>

          </div> <!-- end title container -->

        </div> <!-- end cotnainer -->
      </div> <!-- an-header-banner -->

      <div class="an-page-content">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="content-body">
                <div class="an-section-container pb15">
                  <div class="an-blog-container">
                    <div class="row">
                      <div class="col-md-6 col-sm-6">
                        <div class="an-blog-single"
                          style="background: url({{asset('img/blog/p1.jpg')}}) center center no-repeat;
                          background-size: cover;">
                          <div class="overlay"></div>
                          <div class="content">
                            <h3><a href="blog-post.html">Loren ipsum dolor sit amit.</a></h3>
                            <div class="listing-meta">
                              <span class="user"><span class="meta-italic">By:</span><a href="#">Fiona Wilson</a></span>
                              <span><i class="ion-android-time"></i>16.1.17</span>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6 col-sm-6">
                        <div class="an-blog-single"
                          style="background: url({{asset('img/blog/p2.jpg')}}) center center no-repeat;
                          background-size: cover;">
                          <div class="overlay"></div>
                          <div class="content">
                            <h3><a href="blog-post.html">Loren ipsum dolor sit amit.</a></h3>
                            <div class="listing-meta">
                              <span class="user"><span class="meta-italic">By:</span><a href="#">Fiona Wilson</a></span>
                              <span><i class="ion-android-time"></i>16.1.17</span>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6 col-sm-6">
                        <div class="an-blog-single"
                          style="background: url({{asset('img/blog/p3.jpg')}}) center center no-repeat;
                          background-size: cover;">
                          <div class="overlay"></div>
                          <div class="content">
                            <h3><a href="blog-post.html">Loren ipsum dolor sit amit.</a></h3>
                            <div class="listing-meta">
                              <span class="user"><span class="meta-italic">By:</span><a href="#">Fiona Wilson</a></span>
                              <span><i class="ion-android-time"></i>16.1.17</span>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6 col-sm-6">
                        <div class="an-blog-single">
                          style="background: url({{asset('img/blog/p4.jpg')}}) center center no-repeat;
                          background-size: cover;">
                          <div class="overlay"></div>
                          <div class="content">
                            <h3><a href="blog-post.html">Loren ipsum dolor sit amit.</a></h3>
                            <div class="listing-meta">
                              <span class="user"><span class="meta-italic">By:</span><a href="#">Fiona Wilson</a></span>
                              <span><i class="ion-android-time"></i>16.1.17</span>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6 col-sm-6">
                        <div class="an-blog-single"
                          style="background: url({{asset('img/blog/p5.jpg')}}) center center no-repeat;
                          background-size: cover;">
                          <div class="overlay"></div>
                          <div class="content">
                            <h3><a href="blog-post.html">Loren ipsum dolor sit amit.</a></h3>
                            <div class="listing-meta">
                              <span class="user"><span class="meta-italic">By:</span><a href="#">Fiona Wilson</a></span>
                              <span><i class="ion-android-time"></i>16.1.17</span>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6 col-sm-6">
                        <div class="an-blog-single"
                          style="background: url({{asset('img/blog/p6.jpg')}}) center center no-repeat;
                          background-size: cover;">
                          <div class="overlay"></div>
                          <div class="content">
                            <h3><a href="blog-post.html">Loren ipsum dolor sit amit.</a></h3>
                            <div class="listing-meta">
                              <span class="user"><span class="meta-italic">By:</span><a href="#">Fiona Wilson</a></span>
                              <span><i class="ion-android-time"></i>16.1.17</span>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6 col-sm-6">
                        <div class="an-blog-single"
                          style="background: url('assets/img/blog/p7.jpg') center center no-repeat;
                          background-size: cover;">
                          <div class="overlay"></div>
                          <div class="content">
                            <h3><a href="blog-post.html">Loren ipsum dolor sit amit.</a></h3>
                            <div class="listing-meta">
                              <span class="user"><span class="meta-italic">By:</span><a href="#">Fiona Wilson</a></span>
                              <span><i class="ion-android-time"></i>16.1.17</span>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6 col-sm-6">
                        <div class="an-blog-single"
                          style="background: url({{asset('img/blog/p8.jpg')}}) center center no-repeat;
                          background-size: cover;">
                          <div class="overlay"></div>
                          <div class="content">
                            <h3><a href="blog-post.html">Loren ipsum dolor sit amit.</a></h3>
                            <div class="listing-meta">
                              <span class="user"><span class="meta-italic">By:</span><a href="#">Fiona Wilson</a></span>
                              <span><i class="ion-android-time"></i>16.1.17</span>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6 col-sm-6">
                        <div class="an-blog-single"
                          style="background: url({{asset('img/blog/p9.jpg')}}) center center no-repeat;
                          background-size: cover;">
                          <div class="overlay"></div>
                          <div class="content">
                            <h3><a href="blog-post.html">Loren ipsum dolor sit amit.</a></h3>
                            <div class="listing-meta">
                              <span class="user"><span class="meta-italic">By:</span><a href="#">Fiona Wilson</a></span>
                              <span><i class="ion-android-time"></i>16.1.17</span>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-12">
                        <div class="an-pagination">
                          <a class="an-btn an-btn-default icon-right" href="#">Load More <i class="ion-android-add"></i></a>

                          <ul class="an-paganation-list list-inline">
                            <li><a href="#"><i class="ion-ios-arrow-back"></i>Prev</a></li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">..</a></li>
                            <li><a href="#">45</a></li>
                            <li><a href="#">Next<i class="ion-ios-arrow-forward"></i></a></li>
                          </ul>
                        </div> <!-- an-pagination -->
                      </div>
                    </div> <!-- end row -->
                  </div> <!-- / blog-container -->
                </div> <!-- end an-section-container -->
              </div> <!-- end content-body -->
            </div>
             <!-- end col-md-3 -->
          </div>
        </div>
      </div> <!-- end an-page-content -->

    @endsection
