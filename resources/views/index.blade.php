@extends('layouts.main')

@section('content')

      <div class="an-header-banner has-bg" style="background: url('{{asset("img/home.jpg")}}') center center no-repeat;
        background-size: cover;">
        <div class="home-banner-content">
          <div class="overlay"></div>
          <div class="container">
            <div class="details">
              <h2 class="">TAMANA</h2>
              <h1 class="">Find Your <span>Service.</span></h1>
            </div>




            <div class="an-search-container">
              <div class="an-tab-container">
                <div class="tab-nav">
                  <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#professional" aria-controls="professional" role="tab" data-toggle="tab">Professionals</a></li>
                    <li role="presentation"><a href="#institution" aria-controls="institution" role="tab" data-toggle="tab">Institutions</a></li>
                    <li role="presentation"><a href="#support" aria-controls="support" role="tab" data-toggle="tab">Support</a></li>
                  </ul>
                </div>

                <!-- Tab panes -->
                <div class="tab-content">
                  <div role="tabpanel" class="tab-pane fade in active" id="professional">
                    <form action="/search" method="POST">
                      {{ csrf_field() }}
                      <input type="hidden" name="type" value="professional">
                      <div class="search-fields">
                        <div class="row">
                          <div class="col-md-4">
                            <input class='an-form-control' type="text" name="location" placeholder="Enter location">
                          </div>
                          <div class="col-md-4">
                            <div class="an-default-select-wrapper">
                              <select name="challange">
                                <option value="">--Select (Optional)--</option>
                                <option value="Intellectual Disability">Intellectual Disability</option>
                                <option value="Autism">Autism</option>
                                <option value="Down Syndrome">Down Syndrome</option>
                                <option value="Cerebral Palsy">Cerebral Palsy</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <input class='an-form-control' type="text" name="name" placeholder="Professional's Name">
                          </div>
                          <div class="col-md-12" style="margin-bottom: 10px;">
                            <div class="element-single">
                              <div class="col-md-2">
                                <h4 style="font-weight:bold;text-align:left;">Services:</h4>
                              </div>
                              <div class="col-md-2">
                              <span class="an-custom-checkbox radio dark-text">
                                <input type="radio" name="service" id="check-111" value="" checked>
                                <label for="check-111">All</label>
                              </span>
                            </div>
                              <div class="col-md-2">
                              <span class="an-custom-checkbox radio dark-text">
                                <input type="radio" name="service" id="check-112" value="Occupational Therapy">
                                <label for="check-112">Occupational Therapy</label>
                              </span>
                              </div>
                              <div class="col-md-2">
                              <span class="an-custom-checkbox radio dark-text">
                                <input type="radio" name="service" id="check-113" value="Speech Therapy">
                                <label for="check-113">Speech Therapy</label>
                              </span>
                              </div>
                              <div class="col-md-2">
                              <span class="an-custom-checkbox radio dark-text">
                                <input type="radio" name="service" id="check-114" value="Psychiatry">
                                <label for="check-114">Psychiatry</label>
                              </span>
                              </div>
                              <div class="col-md-2">
                              <span class="an-custom-checkbox radio dark-text">
                                <input type="radio" name="service" id="check-115" value="Physiotherapy">
                                <label for="check-115">Physiotherapy</label>
                              </span>
                              </div>
                              </div>
                            </div>
                              <div class="col-md-2 col-md-offset-2">
                              <span class="an-custom-checkbox radio dark-text">
                                <input type="radio" name="service" id="check-116" value="Paediatricians">
                                <label for="check-116">Paediatricians</label>
                              </span>
                              </div>
                          <div class="col-sm-12">
                            <button type="submit" class="an-btn an-btn-default icon-left fluid"><i class="fa fa-search"></i>Search</button>
                          </div>
                          <!-- end nested row -->
                        </div>
                      </div>
                    </form>
                  </div> <!-- end tab pane -->
                  <div role="tabpanel" class="tab-pane fade rent-pane" id="institution">
                    <form action="/search" method="POST">
                      {{ csrf_field() }}
                      <input type="hidden" name="type" value="institution">
                      <div class="search-fields">
                        <div class="row">
                          <div class="col-md-4">
                            <input class='an-form-control' type="text" name="location" placeholder="Enter location">
                          </div>
                          <div class="col-md-4">
                            <div class="an-default-select-wrapper">
                              <select name="challange">
                                <option value="">--Select (Optional)--</option>
                                <option value="Intellectual Disability">Intellectual Disability</option>
                                <option value="Autism">Autism</option>
                                <option value="Down Syndrome">Down Syndrome</option>
                                <option value="Cerebral Palsy">Cerebral Palsy</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <input class='an-form-control' type="text" name="name" placeholder="Institution's Name">
                          </div>
                          <div class="col-md-12" style="margin-bottom: 10px;">
                            <div class="element-single">
                              <div class="col-md-2">
                                <h4 style="font-weight:bold;text-align:left;">Services:</h4>
                              </div>
                              <div class="col-md-2">
                              <span class="an-custom-checkbox radio dark-text">
                                <input type="radio" name="service" id="check-211" value="" checked>
                                <label for="check-211">All</label>
                              </span>
                            </div>
                              <div class="col-md-2">
                              <span class="an-custom-checkbox radio dark-text">
                                <input type="radio" name="service" value="School" id="check-212">
                                <label for="check-212">School</label>
                              </span>
                              </div>
                              <div class="col-md-2">
                              <span class="an-custom-checkbox radio dark-text">
                                <input type="radio" name="service" value="Residence" id="check-213">
                                <label for="check-213">Residence</label>
                              </span>
                              </div>
                              <div class="col-md-2">
                              <span class="an-custom-checkbox radio dark-text">
                                <input type="radio" name="service" value="Day Care" id="check-214">
                                <label for="check-214">Day Care</label>
                              </span>
                              </div>
                              <div class="col-md-2">
                              <span class="an-custom-checkbox radio dark-text">
                                <input type="radio" name="service" value="Skill Development" id="check-215">
                                <label for="check-215">Skill Development</label>
                              </span>
                              </div>
                            </div>
                          </div>
                              <div class="col-md-2 col-md-offset-2">
                              <span class="an-custom-checkbox radio dark-text">
                                <input type="radio" name="service" value="Diagnostic Center" id="check-216">
                                <label for="check-216">Diagnostic Center</label>
                              </span>
                              </div>
                              <div class="col-md-2">
                              <span class="an-custom-checkbox radio dark-text">
                                <input type="radio" name="service" value="Early Intervention" id="check-217">
                                <label for="check-217">Early Intervention</label>
                              </span>
                              </div>


                          <div class="col-sm-12">
                            <button type="submit" class="an-btn an-btn-default icon-left fluid"><i class="fa fa-search"></i>Search</button>
                          </div>
                          <!-- end nested row -->
                        </div>
                      </div>
                    </form>
                  </div>
                <div role="tabpanel" class="tab-pane fade rent-pane" id="support">
                  <form action="/search" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="type" value="support">
                    <div class="search-fields">
                      <div class="row">
                        <div class="col-md-4">
                          <input class='an-form-control' type="text" name="location" placeholder="Enter location">
                        </div>
                        <div class="col-md-4">
                          <div class="an-default-select-wrapper">
                            <select name="challange">
                              <option value="">--Select (Optional)--</option>
                              <option value="Intellectual Disability">Intellectual Disability</option>
                              <option value="Autism">Autism</option>
                              <option value="Down Syndrome">Down Syndrome</option>
                              <option value="Cerebral Palsy">Cerebral Palsy</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <input class='an-form-control' type="text" name="name" placeholder="Professional's Name">
                        </div>
                        <div class="col-md-12" style="margin-bottom: 10px;">
                          <div class="element-single">
                            <div class="col-md-2">
                              <h4 style="font-weight:bold;text-align:left;">Services:</h4>
                            </div>
                            <div class="col-md-2">
                            <span class="an-custom-checkbox radio dark-text">
                              <input type="radio" name="service" value="" id="check-311" checked>
                              <label for="check-311">All</label>
                            </span>
                          </div>
                            <div class="col-md-2">
                            <span class="an-custom-checkbox radio dark-text">
                              <input type="radio" name="service" value="Counselling" id="check-312">
                              <label for="check-312">Counselling</label>
                            </span>
                            </div>
                            <div class="col-md-2">
                            <span class="an-custom-checkbox radio dark-text">
                              <input type="radio" name="service" value="Lawywers and Advocates" id="check-313">
                              <label for="check-313">Lawywers and Advocates</label>
                            </span>
                            </div>
                            <div class="col-md-2">
                            <span class="an-custom-checkbox radio dark-text">
                              <input type="radio" name="service" value="Daycare Centres" id="check-314">
                              <label for="check-314">Daycare Centres</label>
                            </span>
                            </div>
                            <div class="col-md-2">
                            <span class="an-custom-checkbox radio dark-text">
                              <input type="radio" name="service" value="Parents Support Group" id="check-315">
                              <label for="check-315">Parents Support Group</label>
                            </span>
                            </div>
                          </div>
                          </div>
                            <div class="col-md-2 col-md-offset-2">
                            <span class="an-custom-checkbox radio dark-text">
                              <input type="radio" name="service" value="Special Education" id="check-316">
                              <label for="check-316">Special Education</label>
                            </span>
                            </div>

                        <div class="col-sm-12">
                          <button type="submit" class="an-btn an-btn-default icon-left fluid"><i class="fa fa-search"></i>Search</button>
                        </div>
                        <!-- end nested row -->

                    </div>
                  </form>
                </div> <!-- end tab pane -->

              </div> <!-- an-tab-container -->

            </div> <!-- end an-search-container -->

         <!-- end an-header-banner -->

    <!--  <div class="an-page-content">
        <div class="an-section-container">
          <div class="container">
            <div class="an-title-container">
              <h1 class="an-title">Featured Locations</h1>
              <p class="an-sub-title">Our top location list based on sale and rent property.</p>
            </div> --><!-- end title container -->

          <!-- <div class="an-location-listing">
              <div class="row">
                <div class="an-location-single col-md-3 col-sm-4">
                  <a href="#">
                    <div class="content" style="background: url('{{asset("img/paris.jpg")}}') center center no-repeat;
                      background-size: cover;">
                      <h2>Paris</h2>
                    </div> --><!-- end content -->
                <!--  </a>
                </div>--> <!-- end an-location -single -->
                <!--
                <div class="an-location-single col-md-3 col-sm-4">
                  <a href="#">
                    <div class="content" style="background: url('{{asset("img/london.jpg")}}') center center no-repeat;
                      background-size: cover;">
                      <h2>London</h2>
                    </div>--> <!-- end content -->
                <!--  </a>
                </div> --><!-- end an-location -single -->

              <!--  <div class="an-location-single col-md-3 col-sm-4">
                  <a href="#">
                    <div class="content" style="background: url('{{asset("img/spain.jpg")}}') center center no-repeat;
                      background-size: cover;">
                      <h2>Spain</h2>
                    </div>--> <!-- end content -->
                <!--  </a>
                </div>--> <!-- end an-location -single -->

                <!--<div class="an-location-single col-md-3 col-sm-4">
                  <a href="#">
                    <div class="content" style="background: url('{{asset("img/new-york.jpg")}}') center center no-repeat;
                      background-size: cover;">
                      <h2>New York</h2>
                    </div>--> <!-- end content -->
                  </a>
                </div> <!-- end an-location -single -->
              </div>
            </div>

          </div> <!-- end /container -->
        </div> <!-- end /an-section-container -->

        <!-- <div class="an-section-container  bg-gray pb40">
          <div class="container">
            <div class="an-title-container">
              <h1 class="an-title">How it works</h1>
              <p class="an-sub-title">Find your service.</p>
            </div>

            <div class="row">
              <div class="col-md-3 col-sm-3">
                <div class="an-works-single">
                  <i class="fa fa-search"></i>
                  <h2>Search Services</h2>
                  <p>Search from our extensive directory of service providers.</p>
                </div>
              </div>

              <div class="col-md-3 col-sm-3">
                <div class="an-works-single">
                  <i class="fa fa-user"></i>
                  <h2>Search Professionals</h2>
                  <p>Search for Professional support service providers.</p>
                </div>
              </div>

              <div class="col-md-3 col-sm-3">
                <div class="an-works-single">
                  <i class="fa fa-cubes" style="font-size:48px;color:#3A71FC"></i>
                  <h2>Search Challenges</h2>
                  <p>Search for different challenges.</p>
                </div>
              </div>

              <div class="col-md-3 col-sm-3">
                <div class="an-works-single">
                  <i class="fa fa-map-marker" style="font-size:48px;color:#3A71FC"></i>
                  <h2>Search Location</h2>
                  <p>Search for location.</p>
                </div>
              </div>
            </div>



          </div>
        </div>  -->

      <!--  <div class="an-section-container">
          <div class="container">
            <div class="an-title-container">
              <h1 class="an-title">Featured Listing</h1>
              <p class="an-sub-title">Some of our featured listing.</p>
            </div>--> <!-- end title container -->

          <!--  <div class="row">
              <div class="col-md-4">
                <div class="an-listing-single">
                  <div class="listing-img-container"
                    style="background: url('{{asset("img/listing-1.jpg")}}') center center no-repeat;
                    background-size: cover;">

                    <span class="an-badge bg-primary">Rent</span>
                    <div class="listing-hover-content">
                      <a class="link-single" href="listing-single.html"><i class="ion-forward"></i></a>
                      <p>
                        <span><i class="typcn typcn-eye-outline color-job"></i> 10 234</span>
                        <span><i class="typcn typcn-heart-outline color-accents"></i> 234</span>
                      </p>
                      <button class="an-btn an-btn-black icon-left small-padding"><i class="ion-android-locate"></i>On the map</button>
                    </div>
                  </div>
                  <div class="listing-content">
                    <div class="listing-name">
                      <h3><a href="listing-single.html">Hill lake view house.</a></h3>
                      <div class="price">
                        <p class="color-primary">$900</p>
                        <ul class="an-rating-list list-inline">
                          <li><i class="fa fa-star"></i></li>
                          <li><i class="fa fa-star"></i></li>
                          <li><i class="fa fa-star"></i></li>
                          <li><i class="fa fa-star"></i></li>
                          <li><i class="fa fa-star-half-o"></i></li>
                        </ul>
                      </div>

                    </div>
                    <div class="listing-meta">
                      <span><i class="fa fa-arrows-alt"></i>1600m2</span>
                      <span><i class="fa fa-bathtub"></i>3</span>
                      <span><i class="fa fa-bed"></i>4</span>
                      <span><i class="fa fa-map-marker"></i>143 Ave Str, London</span>
                    </div>
                  </div>
                </div>--> <!-- end listing-single -->
            <!--  </div>

              <div class="col-md-4">
                <div class="an-listing-single">
                  <div class="listing-img-container"
                    style="background: url('{{asset("img/listing-2.jpg")}}') center center no-repeat;
                    background-size: cover;">

                    <span class="an-badge bg-primary">Sale</span>
                    <div class="listing-hover-content">
                      <a class="link-single" href="listing-single.html"><i class="ion-forward"></i></a>
                      <p>
                        <span><i class="typcn typcn-eye-outline color-job"></i> 10 234</span>
                        <span><i class="typcn typcn-heart-outline color-accents"></i> 234</span>
                      </p>
                      <button class="an-btn an-btn-black icon-left small-padding"><i class="ion-android-locate"></i>On the map</button>
                    </div>
                  </div>
                  <div class="listing-content">
                    <div class="listing-name">
                      <h3><a href="listing-single.html">Sweet Snow house for sale.</a></h3>
                      <div class="price">
                        <p class="color-primary">$6 300</p>
                        <ul class="an-rating-list list-inline">
                          <li><i class="fa fa-star"></i></li>
                          <li><i class="fa fa-star"></i></li>
                          <li><i class="fa fa-star"></i></li>
                          <li><i class="fa fa-star"></i></li>
                          <li><i class="fa fa-star-half-o"></i></li>
                        </ul>
                      </div>

                    </div>
                    <div class="listing-meta">
                      <span><i class="fa fa-arrows-alt"></i>1200m2</span>
                      <span><i class="fa fa-bathtub"></i>2</span>
                      <span><i class="fa fa-bed"></i>3</span>
                      <span><i class="fa fa-map-marker"></i>106 B str, Boka roton</span>
                    </div>
                  </div>
                </div>--> <!-- end listing-single -->
            <!--  </div>

              <div class="col-md-4">
                <div class="an-listing-single">
                  <div class="listing-img-container"
                    style="background: url('{{asset("img/listing-3.jpg")}}') center center no-repeat;
                    background-size: cover;">

                    <span class="an-badge bg-primary">Sale</span>
                    <div class="listing-hover-content">
                      <a class="link-single" href="listing-single.html"><i class="ion-forward"></i></a>
                      <p>
                        <span><i class="typcn typcn-eye-outline color-job"></i> 10 234</span>
                        <span><i class="typcn typcn-heart-outline color-accents"></i> 234</span>
                      </p>
                      <button class="an-btn an-btn-black icon-left small-padding"><i class="ion-android-locate"></i>On the map</button>
                    </div>
                  </div>
                  <div class="listing-content">
                    <div class="listing-name">
                      <h3><a href="listing-single.html">Road side villa.</a></h3>
                      <div class="price">
                        <p class="color-primary">$6 300</p>
                        <ul class="an-rating-list list-inline">
                          <li><i class="fa fa-star"></i></li>
                          <li><i class="fa fa-star"></i></li>
                          <li><i class="fa fa-star"></i></li>
                          <li><i class="fa fa-star"></i></li>
                          <li><i class="fa fa-star"></i></li>
                        </ul>
                      </div>

                    </div>
                    <div class="listing-meta">
                      <span><i class="fa fa-arrows-alt"></i>1200m2</span>
                      <span><i class="fa fa-bathtub"></i>2</span>
                      <span><i class="fa fa-bed"></i>3</span>
                      <span><i class="fa fa-map-marker"></i>106 B str, Boka roton</span>
                    </div>
                  </div>
                </div> --><!-- end listing-single -->
            <!--  </div>

              <div class="col-md-4">
                <div class="an-listing-single">
                  <div class="listing-img-container"
                    style="background: url('{{asset("img/listing-1.jpg")}}') center center no-repeat;
                    background-size: cover;">

                    <span class="an-badge bg-primary">Rent</span>
                    <div class="listing-hover-content">
                      <a class="link-single" href="listing-single.html"><i class="ion-forward"></i></a>
                      <p>
                        <span><i class="typcn typcn-eye-outline color-job"></i> 10 234</span>
                        <span><i class="typcn typcn-heart-outline color-accents"></i> 234</span>
                      </p>
                      <button class="an-btn an-btn-black icon-left small-padding"><i class="ion-android-locate"></i>On the map</button>
                    </div>
                  </div>
                  <div class="listing-content">
                    <div class="listing-name">
                      <h3><a href="listing-single.html">Hill lake view house.</a></h3>
                      <div class="price">
                        <p class="color-primary">$900</p>
                        <ul class="an-rating-list list-inline">
                          <li><i class="fa fa-star"></i></li>
                          <li><i class="fa fa-star"></i></li>
                          <li><i class="fa fa-star"></i></li>
                          <li><i class="fa fa-star"></i></li>
                          <li><i class="fa fa-star-half-o"></i></li>
                        </ul>
                      </div>

                    </div>
                    <div class="listing-meta">
                      <span><i class="fa fa-arrows-alt"></i>1600m2</span>
                      <span><i class="fa fa-bathtub"></i>3</span>
                      <span><i class="fa fa-bed"></i>4</span>
                      <span><i class="fa fa-map-marker"></i>143 Ave Str, London</span>
                    </div>
                  </div>
                </div>--> <!-- end listing-single -->
            <!--  </div>

              <div class="col-md-4">
                <div class="an-listing-single">
                  <div class="listing-img-container"
                    style="background: url('{{asset("img/listing-2.jpg")}}') center center no-repeat;
                    background-size: cover;">

                    <span class="an-badge bg-primary">Sale</span>
                    <div class="listing-hover-content">
                      <a class="link-single" href="listing-single.html"><i class="ion-forward"></i></a>
                      <p>
                        <span><i class="typcn typcn-eye-outline color-job"></i> 10 234</span>
                        <span><i class="typcn typcn-heart-outline color-accents"></i> 234</span>
                      </p>
                      <button class="an-btn an-btn-black icon-left small-padding"><i class="ion-android-locate"></i>On the map</button>
                    </div>
                  </div>
                  <div class="listing-content">
                    <div class="listing-name">
                      <h3><a href="listing-single.html">Sweet Snow house for sale.</a></h3>
                      <div class="price">
                        <p class="color-primary">$6 300</p>
                        <ul class="an-rating-list list-inline">
                          <li><i class="fa fa-star"></i></li>
                          <li><i class="fa fa-star"></i></li>
                          <li><i class="fa fa-star"></i></li>
                          <li><i class="fa fa-star"></i></li>
                          <li><i class="fa fa-star-half-o"></i></li>
                        </ul>
                      </div>

                    </div>
                    <div class="listing-meta">
                      <span><i class="fa fa-arrows-alt"></i>1200m2</span>
                      <span><i class="fa fa-bathtub"></i>2</span>
                      <span><i class="fa fa-bed"></i>3</span>
                      <span><i class="fa fa-map-marker"></i>106 B str, Boka roton</span>
                    </div>
                  </div>
                </div>--> <!-- end listing-single -->
            <!--  </div>

              <div class="col-md-4">
                <div class="an-listing-single">
                  <div class="listing-img-container"
                    style="background: url('{{asset("img/listing-3.jpg")}}') center center no-repeat;
                    background-size: cover;">

                    <span class="an-badge bg-primary">Sale</span>
                    <div class="listing-hover-content">
                      <a class="link-single" href="listing-single.html"><i class="ion-forward"></i></a>
                      <p>
                        <span><i class="typcn typcn-eye-outline color-job"></i> 10 234</span>
                        <span><i class="typcn typcn-heart-outline color-accents"></i> 234</span>
                      </p>
                      <button class="an-btn an-btn-black icon-left small-padding"><i class="ion-android-locate"></i>On the map</button>
                    </div>
                  </div>
                  <div class="listing-content">
                    <div class="listing-name">
                      <h3><a href="listing-single.html">Road side villa.</a></h3>
                      <div class="price">
                        <p class="color-primary">$6 300</p>
                        <ul class="an-rating-list list-inline">
                          <li><i class="fa fa-star"></i></li>
                          <li><i class="fa fa-star"></i></li>
                          <li><i class="fa fa-star"></i></li>
                          <li><i class="fa fa-star"></i></li>
                          <li><i class="fa fa-star"></i></li>
                        </ul>
                      </div>

                    </div>
                    <div class="listing-meta">
                      <span><i class="fa fa-arrows-alt"></i>1200m2</span>
                      <span><i class="fa fa-bathtub"></i>2</span>
                      <span><i class="fa fa-bed"></i>3</span>
                      <span><i class="fa fa-map-marker"></i>106 B str, Boka roton</span>
                    </div>
                  </div>
                </div> --><!-- end listing-single -->
            <!--  </div>

              <div class="col-md-12 text-center pt15 pb30">
                <a href="#" class="an-btn an-btn-default">View More</a>
              </div>
            </div>

          </div>--> <!-- end /container -->
        <!--</div>--> <!-- end /an-section-container -->

        <div class="an-section-container bg-gray">

          <div class="row">
            <div class="panel panel-default">
              &nbsp;<br />
              &nbsp;<br />
              <div class="an-title-container">
                <h1 class="an-title">Narratives</h1>
                <p class="an-sub-title">Watch our videos</p>
              </div> <!-- end title container -->
              <div class="panel-body">
                <div class="col-md-6" style="border-right:1px solid #3A71FC">
                  <center><div class="vid-container">
    	               <iframe width="460" height="315" src="https://www.youtube.com/embed/videoseries?list=PLx0sYbCqOb8TBPRdmBHs5Iftvv9TPboYG" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <h4><b>Info:</b></h4>
                      <p>ehafkjdghekagsdhakjdshkwjhakjsdhwkjahdkjashd</p>
                      &nbsp;<br />
                      &nbsp;<br />
                      &nbsp;<br />
                      &nbsp;<br />
                      &nbsp;<br />
                      &nbsp;<br />
                      &nbsp;<br />
                      &nbsp;<br />
                    </div>
                    <div class="col-md-6">
                      <div class="pull-right">
                        <a href="{{route('narratives')}}"<button class="an-btn an-btn-default rounded" >See More</button></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>


          <!-- Events -->

          <div class="an-section-container pb0">
            <div class="an-title-container">
              <h1 class="an-title">Events</h1>
              <p class="an-sub-title">Upcoming events at a glance</p>
            </div> <!-- end title container -->
            <div class="an-news-container">
              <div class="container">
                <div class="row">
                  <div class="col-sm-12">

                    <div class="element-single">

                      <div class="an-pricing">
                        <div class="row">
                          <div class="col-md-4 col-sm-6">
                            <div class="an-pricing-table-single with-shadow-dark wow fadeIn" data-wow-delay=".1s">
                              <div class="price-header">
                                <h3 class="plan-name">Art Conference</h3>
                                <p>Satrting On</p>
                                <p>13th March, 2018</p>
                                <h1 class="plan-price"><span><i class="fa fa-calendar"></i></span>08:15 AM</h1>
                              </div>
                              <div class="helper-text">
                                <p>Art Festival by Tamana will be hold on at Tamana Institution, Visant Bihar</p>
                              </div>
                              <ul class="feature-lists">
                                <li>Speech of Directors  ( 08:30 AM - 9:00 AM )</li>
                                <li>Art Competetion ( 9:00 AM - 1:00 PM ) </li>
                                <li>Snacks Time ( 1:00 PM - 2:00 PM )</li>
                              </ul>
                            </div>

                            <!-- end .AN-SINGEL-uSEr-MAIN-BODY -->
                          </div>

                          <div class="col-md-4 col-sm-6">
                            <div class="an-pricing-table-single with-shadow-dark wow fadeIn" data-wow-delay=".1s">
                              <div class="price-header">
                                <h3 class="plan-name">Art Conference</h3>
                                <p>Satrting On</p>
                                <p>13th March, 2018</p>
                                <h1 class="plan-price"><span><i class="fa fa-calendar"></i></span>08:15 AM</h1>
                              </div>
                              <div class="helper-text">
                                <p>Art Festival by Tamana will be hold on at Tamana Institution, Visant Bihar</p>
                              </div>
                              <ul class="feature-lists">
                                <li>Speech of Directors  ( 08:30 AM - 9:00 AM )</li>
                                <li>Art Competetion ( 9:00 AM - 1:00 PM ) </li>
                                <li>Snacks Time ( 1:00 PM - 2:00 PM )</li>
                              </ul>
                            </div>

                            <!-- end .AN-SINGEL-uSEr-MAIN-BODY -->
                          </div>

                          <div class="col-md-4 col-sm-6">
                            <div class="an-pricing-table-single with-shadow-dark wow fadeIn" data-wow-delay=".1s">
                              <div class="price-header">
                                <h3 class="plan-name">Art Conference</h3>
                                <p>Satrting On</p>
                                <p>13th March, 2018</p>
                                <h1 class="plan-price"><span><i class="fa fa-calendar"></i></span>08:15 AM</h1>
                              </div>
                              <div class="helper-text">
                                <p>Art Festival by Tamana will be hold on at Tamana Institution, Visant Bihar</p>
                              </div>
                              <ul class="feature-lists">
                                <li>Speech of Directors  ( 08:30 AM - 9:00 AM )</li>
                                <li>Art Competetion ( 9:00 AM - 1:00 PM ) </li>
                                <li>Snacks Time ( 1:00 PM - 2:00 PM )</li>
                              </ul>
                            </div>

                            <!-- end .AN-SINGEL-uSEr-MAIN-BODY -->
                          </div>

                        </div> <!-- end row -->
                      </div>
                    </div>
                    <!-- end row -->

                  </div>
                </div>
                <div class="col-md-12">
                  <center>
                    <a href="{{route('narratives')}}"<button class="an-btn an-btn-default rounded" >See More</button></a>
                  </center>
                </div>
              </div> <!-- end an-news-container -->
            </div>

          </div> <!-- end an-section-contaienr -->


          <!-- end Events -->

          <hr>




        <div class="an-section-container pb0">
          <div class="an-title-container">
            <h1 class="an-title">Stories</h1>
            <p class="an-sub-title">Read our latest stories</p>
          </div> <!-- end title container -->
          <div class="an-news-container">
            <div class="container">
              <div class="row">
                @foreach($stories as $story)
                <div class="col-sm-6">

                  <div class="an-news-single with-thumb">
                    <img src="{{asset('img/blog/p1.jpg')}}" alt="">
                    <a href="{{route('stories.details',[$story->id])}}">{{$story->title}}</a>
                    <div class="listing-meta">
                      <span><i class="fa fa-clock-o"></i>16.1.17</span>
                      <span><i class="fa fa-user"></i>{{$story->name}}</span>
                    </div>
                  </div> <!-- end an-news-single -->

                </div>
                @endforeach
              </div>
              <div class="col-md-12">
                  <center><a href="{{route('stories.index')}}"<button class="an-btn an-btn-default rounded" >See More</button></a></center>
              </div>
            </div> <!-- end an-news-container -->
          </div>

        </div> <!-- end an-section-contaienr -->
      </div> <!-- end an-page-content -->

@endsection
