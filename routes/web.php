<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/','IndexController@index')->name('main.index');

//Route::get('/admin', 'Admin\AdminController@index')->name('admin.index');



Auth::routes();
Route::post('/search', 'SearchController@search')->name('search');
Route::get('/contact', 'ContactController@index')->name('contact');
Route::get('/about', 'AboutController@index')->name('about');
Route::get('/narratives', 'NarrativeController@index')->name('narratives');
Route::get('/details/{id}/{type}','SearchController@details')->name('details');
Route::get('/stories', 'StoriesController@index')->name('stories.index');
Route::get('/stories/details/{id}','StoriesController@details')->name('stories.details');
Route::get('/events','EventController@index')->name('events.details');
Route::get('/write','WriteController@index')->name('write.index');
Route::get('/write/story','WriteController@write')->name('student.stories.write');
Route::post('/write/story','WriteController@store')->name('stories.store');
