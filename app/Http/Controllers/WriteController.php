<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;



class WriteController extends Controller
{
    //
    public function __construct(){
      $this->middleware('auth');
    }

    public function index(){
      return view('student.demo');
    }
    public function write(){
      return view('student.write');
    }
    public function store(Request $request){
      $this->validate($request,[
        'name' => 'required',
        'title' => 'required',
        'story' => 'required',
        ]);

        DB::table('stories')->insert(['name'=> $request->input('name'),'title'=>$request->input('title'),'story'=>$request->input('story')]);
        Session::flash('success', 'Story Added');

        return redirect()->route('student.stories.write');

    }
}
