<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;

class IndexController extends Controller
{
    //
    public function index(){
      $stories=DB::table('stories')->orderBy('id','DESC')->take(6)->get();

      return view('index')->withStories($stories);
    }
}
