<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;


class SearchController extends Controller
{
    //
    public function search(Request $request)
    {
      if($request->input('type')=='professional'){
        $data= DB::table('professionals')->where('location','like','%'.$request->input('location').'%')
                                          ->where('name','like','%'.$request->input('name').'%')
                                          ->get();
        if($request->input('challange')==''){
          if($request->input('service')==''){
            return view('result')->withLocation($request->input('location'))
                                ->withName($request->input('name'))
                                ->withChallange($request->input('challange'))
                                ->withData($data)
                                ->withType('professional')
                                ->withService($request->input('service'));
          }
          else{
            $new_res=array();
            foreach($data as $dt){
              $spec=json_decode($dt->specialities);
              for($i=0;$i<sizeof($spec);$i++){
                if($spec[$i]==$request->input('service')){
                  array_push($new_res,$dt);
                }
              }
            }
            return view('result')->withLocation($request->input('location'))
                                ->withName($request->input('name'))
                                ->withChallange($request->input('challange'))
                                ->withData($new_res)
                                ->withType('professional')
                                ->withService($request->input('service'));
          }
        }
        else{
          $new_res2=array();
          foreach($data as $dt){
            $spec2=json_decode($dt->challanges);
            for($i=0;$i<sizeof($spec2);$i++){
              if($spec2[$i]==$request->input('challange')){
                array_push($new_res2,$dt);
              }
            }
          }
          if($request->input('service')==''){
            return view('result')->withLocation($request->input('location'))
                                ->withName($request->input('name'))
                                ->withChallange($request->input('challange'))
                                ->withData($new_res2)
                                ->withType('professional')
                                ->withService($request->input('service'));
          }
          else{
            $data=$new_res2;
            $new_res3=array();
            foreach($data as $dt){
              $spec=json_decode($dt->specialities);
              for($i=0;$i<sizeof($spec);$i++){
                if($spec[$i]==$request->input('service')){
                  array_push($new_res3,$dt);
                }
              }
            }
            return view('result')->withLocation($request->input('location'))
                                ->withName($request->input('name'))
                                ->withChallange($request->input('challange'))
                                ->withData($new_res3)
                                ->withType('professional')
                                ->withService($request->input('service'));
          }
        }
      }


      if($request->input('type')=='institution'){
        $data= DB::table('institutions')->where('location','like','%'.$request->input('location').'%')
                                          ->where('name','like','%'.$request->input('name').'%')
                                          ->get();
        if($request->input('challange')==''){
          if($request->input('service')==''){
            return view('result')->withLocation($request->input('location'))
                                ->withName($request->input('name'))
                                ->withChallange($request->input('challange'))
                                ->withData($data)
                                ->withType('institution')
                                ->withService($request->input('service'));
          }
          else{
            $new_res=array();
            foreach($data as $dt){
              $spec=json_decode($dt->specialities);
              for($i=0;$i<sizeof($spec);$i++){
                if($spec[$i]==$request->input('service')){
                  array_push($new_res,$dt);
                }
              }
            }
            return view('result')->withLocation($request->input('location'))
                                ->withName($request->input('name'))
                                ->withChallange($request->input('challange'))
                                ->withData($new_res)
                                ->withType('institution')
                                ->withService($request->input('service'));
          }
        }
        else{
          $new_res2=array();
          foreach($data as $dt){
            $spec2=json_decode($dt->challanges);
            for($i=0;$i<sizeof($spec2);$i++){
              if($spec2[$i]==$request->input('challange')){
                array_push($new_res2,$dt);
              }
            }
          }
          if($request->input('service')==''){
            return view('result')->withLocation($request->input('location'))
                                ->withName($request->input('name'))
                                ->withChallange($request->input('challange'))
                                ->withData($new_res2)
                                ->withType('institution')
                                ->withService($request->input('service'));
          }
          else{
            $data=$new_res2;
            $new_res3=array();
            foreach($data as $dt){
              $spec=json_decode($dt->specialities);
              for($i=0;$i<sizeof($spec);$i++){
                if($spec[$i]==$request->input('service')){
                  array_push($new_res3,$dt);
                }
              }
            }
            return view('result')->withLocation($request->input('location'))
                                ->withName($request->input('name'))
                                ->withChallange($request->input('challange'))
                                ->withData($new_res3)
                                ->withType('institution')
                                ->withService($request->input('service'));
          }
        }
      }

      if($request->input('type')=='support'){
        $data= DB::table('supports')->where('location','like','%'.$request->input('location').'%')
                                          ->where('name','like','%'.$request->input('name').'%')
                                          ->get();
        if($request->input('challange')==''){
          if($request->input('service')==''){
            return view('result')->withLocation($request->input('location'))
                                ->withName($request->input('name'))
                                ->withChallange($request->input('challange'))
                                ->withData($data)
                                ->withType('support')
                                ->withService($request->input('service'));
          }
          else{
            $new_res=array();
            foreach($data as $dt){
              $spec=json_decode($dt->specialities);
              for($i=0;$i<sizeof($spec);$i++){
                if($spec[$i]==$request->input('service')){
                  array_push($new_res,$dt);
                }
              }
            }
            return view('result')->withLocation($request->input('location'))
                                ->withName($request->input('name'))
                                ->withChallange($request->input('challange'))
                                ->withData($new_res)
                                ->withType('support')
                                ->withService($request->input('service'));
          }
        }
        else{
          $new_res2=array();
          foreach($data as $dt){
            $spec2=json_decode($dt->challanges);
            for($i=0;$i<sizeof($spec2);$i++){
              if($spec2[$i]==$request->input('challange')){
                array_push($new_res2,$dt);
              }
            }
          }
          if($request->input('service')==''){
            return view('result')->withLocation($request->input('location'))
                                ->withName($request->input('name'))
                                ->withChallange($request->input('challange'))
                                ->withData($new_res2)
                                ->withType('support')
                                ->withService($request->input('service'));
          }
          else{
            $data=$new_res2;
            $new_res3=array();
            foreach($data as $dt){
              $spec=json_decode($dt->specialities);
              for($i=0;$i<sizeof($spec);$i++){
                if($spec[$i]==$request->input('service')){
                  array_push($new_res3,$dt);
                }
              }
            }
            return view('result')->withLocation($request->input('location'))
                                ->withName($request->input('name'))
                                ->withChallange($request->input('challange'))
                                ->withData($new_res3)
                                ->withType('support')
                                ->withService($request->input('service'));
          }
        }
      }
    }

    public function details($id,$type){
      if($type=='professional'){
        $data=DB::table('professionals')->where('id',$id)->first();
      }
      if($type=='institution'){
        $data=DB::table('institutions')->where('id',$id)->first();
      }
      if($type=='support'){
        $data=DB::table('supports')->where('id',$id)->first();
      }
      return view('details')->withData($data);
    }
}
